const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

//api/fighters
router.get(
    '/',
    (req, res) => {
        try {
            const fighters = FighterService.searchAllFighters();
            res.status(200).json({ message: 'get all figthers!', fighters });
            res.data = fighters;
        } catch (err) {
            res.err = err;
        } finally {
            next();
        }
    },
    responseMiddleware
);

// get api/fighters/:id
router.get(
    '/:id',
    (req, res) => {
        try {
            const { id } = req.params;

            const getFighterById = FighterService.search({ id: id });

            res.status(200).json({ message: 'Fighter find succesfull!', getFighterById });
            res.data = getFighterById;
        } catch (err) {
            res.err = err;
        } finally {
            next();
        }
    },
    responseMiddleware
);

// post api/fighters
router.post(
    '/',
    createFighterValid,
    (req, res) => {
        try {
            const { name, health, power, defense } = req.body;

            const fighterExist = FighterService.search({ name });
                                   
            if (fighterExist) {
                res.status(400).json({ error: true, message: 'Figther with this name already exist' });
                next();
            }

            const createFighter = FighterService.saveFighter({ name, health, power, defense });

            res.status(200).json({ message: 'Fighter created!', createFighter });
            res.data = createFighter;

            next();
        } catch (err) {
            res.err = err;
        }
    },
    responseMiddleware
);

// put api/fighters/:id
router.put(
    '/:id',
    updateFighterValid,
    (req, res) => {
        try {
            const { id } = req.params;
            const { name, health, power, defense } = req.body;

            const updateFighter = FighterService.updateFighter(id, { name, health, power, defense });

            res.status(200).json({ message: 'Update Fighter succesfull!', updateFighter });
            res.data = updateFighter;
            next();
        } catch (err) {
            res.err = err;
        }
    },
    responseMiddleware
);

// delete api/fighters/:id
router.delete(
    '/:id',
    (req, res) => {
        try {
            const { id } = req.params;

            const fighterToDelete = FighterService.search({ id: id });

            if (!fighterToDelete || null) {
                res.status(400).json({ error: true, message: 'Cant find Fighter to delete' });
            }

            const deleteFighterById = FighterService.deleteFighter(id);

            res.status(200).json({ message: 'Delete Fighter succesfull!', data: deleteFighterById });
            res.data = deleteFighterById;
        } catch (err) {
            res.err = err;
        } finally {
            next();
        }
    },
    responseMiddleware
);

module.exports = router;
