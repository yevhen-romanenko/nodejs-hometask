const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

// get api/users
router.get(
    '/',
    (req, res) => {
        try {
            const users = UserService.searchAllUsers();
            res.status(200).json({ message: 'get all users!', users });
            res.data = users;
        } catch (err) {
            res.err = err;
        } finally {
            next();
        }
    },
    responseMiddleware
);

// get api/users/:id
router.get(
    '/:id',
    (req, res) => {
        try {
            const { id } = req.params;
            const getUserById = UserService.search({ id: id });

            if (!getUserById || null) {
                res.status(404).json({ error: true, message: 'Cant find User by ID' });
            }
            res.status(200).json({ message: 'User find succesfull!', getUserById });
            res.data = getUserById;
        } catch (err) {
            res.err = err;
        } finally {
            next();
        }
    },
    responseMiddleware
);

// post api/users
router.post(
    '/',
    createUserValid,
    (req, res) => {
        try {
            const { firstName, lastName, email, phoneNumber, password } = req.body;

            const emailExist = UserService.search({ email });
            if (emailExist) {
                res.status(400).json({ error: true, message: 'User with that email already exist' });
                next();
            }

            const phoneExist = UserService.search({ phoneNumber });
            if (phoneExist) {
                res.status(400).json({ error: true, message: 'User with that phone already exist' });
                next();
            }

            const createUser = UserService.saveUser({ firstName, lastName, email, phoneNumber, password });

            res.status(200).json({ message: 'User created!', createUser });
            res.data = createUser;

            next();
        } catch (err) {
            res.err = err;
        }
    },
    responseMiddleware
);

// put api/users/:id
router.put(
    '/:id',
    updateUserValid,
    (req, res) => {
        try {
            const { id } = req.params;

            const updateUserById = UserService.search({ id: id });

            if (!updateUserById || null) {
                res.status(400).json({ error: true, message: 'Cant find User by ID' });
            }

            const { firstName, lastName, email, phoneNumber, password } = req.body;

            const updateUser = UserService.updateUser(id, { firstName, lastName, email, phoneNumber, password });

            res.status(200).json({ message: `User ${updateUser.mail} updated succesfull!`, updateUser });
            res.data = updateUser;
            next();
        } catch (err) {
            res.err = err;
        }
    },
    responseMiddleware
);

// delete api/users/:id
router.delete(
    '/:id',
    (req, res) => {
        try {
            const { id } = req.params;

            const deleteUserById = UserService.deleteUser(id);

            res.status(200).json({ message: `User ${deleteUserById.mail} deleted succesfull!`, deleteUserById });
            res.data = deleteUserById;
        } catch (err) {
            res.err = err;
        } finally {
            next();
        }
    },
    responseMiddleware
);

module.exports = router;
