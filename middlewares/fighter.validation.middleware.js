const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    const { name, health, power, defense } = req.body;

    if (isEmpty(name)) {
        res.status(400).json({
            error: true,
            message: 'Name field cannot be empty',
        });
        return null;
    }

    if (!isPowerValid(power)) {
        res.status(400).json({
            error: true,
            message: 'power from 2 to 99 points!',
        });
        return null;
    }

    if (!isHealthValid(health)) {
        res.status(400).json({
            error: true,
            message: 'health range from 80 to 120',
        });
        return null;
    }

    if (!isDefenseValid(defense)) {
        res.status(400).json({
            error: true,
            message: 'Defense stats from 1 to 10 points!',
        });
        return null;
    }

    next();
};

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    const { name, health, power, defense } = req.body;
    
    if (isEmpty(name)) {
        res.status(400).json({
            error: true,
            message: 'Name field cannot be empty',
        });
        return null;
    }

    if (!isPowerValid(power)) {
        res.status(400).json({
            error: true,
            message: 'maximum power 99 points!',
        });
        return null;
    }

    if (!isHealthValid(health)) {
        res.status(400).json({
            error: true,
            message: 'health range from 80 to 120 hp!',
        });
        return null;
    }

    if (!isDefenseValid(defense)) {
        res.status(400).json({
            error: true,
            message: 'Defense stats from 1 to 10 points!',
        });
        return null;
    }
    next();
};

function isPowerValid(power) {
    const powerInt = parseInt(power);
    if (powerInt > 1 && powerInt < 100) {
        return power;
    } else return false;
}

function isHealthValid(health) {
    const healthInt = parseInt(health);
    if (healthInt > 80 && healthInt < 120) {
        return health;
    } else return false;
}

function isDefenseValid(defense) {
    const defenseInt = parseInt(defense);
    if (defenseInt > 1 && defenseInt < 10) {
        return defense;
    } else return false;
}

function isEmpty(field) {
    const checkField = field;

    if (checkField === '' || checkField === undefined) {
        return true;
    }
    // return field;
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
