const responseMiddleware = (req, res, next) => {
    // TODO: Implement middleware that returns result of the query
    if (res.err) {
        if (res.err.message.includes('not found')) {
            res.status(404).json({
                error: true,
                message: res.err.message,
            });
        } else {
            res.status(400).json({
                error: true,
                message: res.err.message,
            });
        }
    }
    if (res.data) {
        res.status(200).json({
            data: res.data,
            message: 'get data!',
        });
    }
    next();
};

exports.responseMiddleware = responseMiddleware;
