const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    const { firstName, lastName, email, phoneNumber, password } = req.body;

    if (isEmpty(firstName)) {
        res.status(400).json({
            error: true,
            message: 'Name field cannot be empty',
        });
        return null;
    }

    if (isEmpty(lastName)) {
        res.status(400).json({
            error: true,
            message: 'LastName field cannot be empty',
        });
        return null;
    }

    if (!isGmailValid(email)) {
        res.status(400).json({
            error: true,
            message: 'email must contain @gmail.com',
        });
        return null;
    }

    if (!isPhoneNumberValid(phoneNumber)) {
        res.status(400).json({
            error: true,
            message: 'phone number format: +380xxxxxxxxx',
        });
        return null;
    }

    if (isEmpty(password)) {
        res.status(400).json({
            error: true,
            message: 'password field cannot be empty',
        });
        return null;
    }

    if (isPasswordValid(password) < 3) {
        res.status(400).json({
            error: true,
            message: 'password field required 3 or more symbols',
        });
        return null;
    }

    next();
};

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update

    const { firstName, lastName, email, phoneNumber, password } = req.body;

    if (isEmpty(firstName)) {
        res.status(400).json({
            error: true,
            message: 'Name field cannot be empty',
        });
        return null;
    }

    if (isEmpty(lastName)) {
        res.status(400).json({
            error: true,
            message: 'LastName field cannot be empty',
        });
        return null;
    }

    if (!isGmailValid(email)) {
        res.status(400).json({
            error: true,
            message: 'email must contain @gmail.com',
        });
        return null;
    }

    if (!isPhoneNumberValid(phoneNumber)) {
        res.status(400).json({
            error: true,
            message: 'phone number format: +380xxxxxxxxx',
        });
        return null;
    }

    if (isEmpty(password)) {
        res.status(400).json({
            error: true,
            message: 'password field cannot be empty',
        });
        return null;
    }

    if (isPasswordValid(password) < 3) {
        res.status(400).json({
            error: true,
            message: 'password field required 3 or more symbols',
        });
        return null;
    }
    next();
};

function isGmailValid(mail) {
    const re = /.+@gmail\.com$/;
    return re.test(mail);
}

function isPhoneNumberValid(number) {
    const re = /^\+?3?8?(0\d{9})$/;
    return re.test(number);
}

function isEmpty(field) {
    const checkField = field;
    if (checkField === '' || checkField === undefined) {
        return true;
    }
}

function isPasswordValid(password) {
    if (password !== undefined) {
        const pwLength = password.length;
        return pwLength;
    }
}

function isAllFieldsValid(data) {
    const fieldsInRequest = Object.keys(data);

    fieldsInRequest.forEach((key) => {
        if (!user.hasOwnProperty(key)) {
            console.log('exist?', user.hasOwnProperty(key));
            return false;
        }
    });
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
