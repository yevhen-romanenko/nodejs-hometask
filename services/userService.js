const { UserRepository } = require('../repositories/userRepository');

class UserService {
    // TODO: Implement methods to work with user
    saveUser(data) {
        const itemdata = UserRepository.create(data);

        if (!itemdata) {
            console.log('data User not found');
            return null;
        }

        return itemdata;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    searchAllUsers() {
        const items = UserRepository.getAll();
        
        return items;
    }

    updateUser(id, data) {
        const userExist = this.search(data.id);
        if (!userExist) {
            return null;
        }

        const userItem = UserRepository.update(id, data);
        return userItem;
    }

    deleteUser(id) {
        const deleteItem = UserRepository.delete(id);
        return deleteItem;
    }
}

module.exports = new UserService();
