const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    saveFighter(data) {
        const itemdata = FighterRepository.create(data);
        if (!itemdata) {
            console.log('data Fighter not found');
            return null;
        }
        return itemdata;
    }

    searchAllFighters() {
        const items = FighterRepository.getAll();
        
        return items;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    updateFighter(id, data) {
        const fighterExist = this.search(data.id);
        if (!fighterExist) {
            return null;
        }

        const updateItem = FighterRepository.update(id, data);
        return updateItem;
    }

    deleteFighter(id) {
        const deleteItem = FighterRepository.delete(id);
        return deleteItem;
    }
}

module.exports = new FighterService();
